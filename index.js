var express = require('express');
var app = express();
var request = require('request');
var nodemailer = require('nodemailer');
var bodyParser = require('body-parser');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use('/', express.static('app'));

app.get('/success',function(req, res) {
  // Use res.sendfile, as it streams instead of reading the file into memory.
  res.sendFile(__dirname+'/app/components/successful_message/success.html');
});

app.get('/googlemaps', function(req, res) {

  var key = req.query.key,
    input = req.query.input;

  request('https://maps.googleapis.com/maps/api/place/autocomplete/json?key=' + key + '&input=' + input, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      res.send(body) // Show the HTML for the Google homepage. 
    }
  })

});

app.use('/mail', function composeMessage(req, res, next){
	var body = req.body.params;

	var text = `
		<h1 style='color:gray'> Briefing </h1>	
		<h2 style='color:gray'>1) Basic Info</h2>
	`;

	body.personalData.forEach(function(data, index){
		text += '<b>'+data.field+': </b>'+data.content+'<br>';

		if(data.field.toLowerCase() == 'e-mail'){
			req.body.personalData = {};
			req.body.personalData.email = data.content;
		}
	})


	body.topics.forEach(function(topic, i){
		i = i + 2;
		text += '<h2 style="color:gray">'+i+') '+topic.subject+'</h2>';

		topic.questions.forEach(function(item, j){
			text += '<h3>'+item.question+'</h3>';
			text += '<p>'+item.answer+'</p>';
		});
	
	});	

	req.body.text = text;

	next();
});

app.post('/mail', function(req, res) {

  var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL 
    auth: {
      user: 'helloncanella@gmail.com',
      pass: 'izzphikkcppatxeq'
    }
  }


  // create reusable transporter object using the default SMTP transport 
  var transporter = nodemailer.createTransport(smtpConfig);

  // setup e-mail data with unicode symbols 
  var mailOptions = {
    to: ['helloncanella@gmail.com', 'farah.alx@gmail.com', req.body.personalData.email], // list of receivers 
    subject: 'Briefing', // Subject line 
    html: req.body.text // html body 
  };

  // send mail with defined transport object 
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      return console.log(error);
    }    

    res.send('ok')
  });

  


})

app.listen(9000);
