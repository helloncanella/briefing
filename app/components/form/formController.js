angular.module('formModule',[])

.controller('formController', ['$scope', '$http', '$window', function($scope, $http, $window) {

  $scope.cities = [];

  $scope.getCities = function(field, content) {
    if (field.toLowerCase() == 'city') {

      $http.get('/googlemaps', {
        params: {
          key: 'AIzaSyC0ohLvz-xdYdKmIlnSAImQZsEZFjCrqGE',
          input: content,
        }
      }).then(function(response) {
        var predictions = response.data.predictions;

        predictions.forEach(function(content, index) {
          $scope.cities[index] = content.description;
        })


      });

    }

  };

  $scope.send = function() {
    $http.post('/mail', {
      params: {
        personalData: $scope.personalData,
        topics: $scope.topics
      }

    }).then(function(response) {
      if(response.status == 200){
        $window.location.href="/success"
      }
    })
  }

  $scope.personalData = [
    { field: 'Name', type: 'text' },
    { field: 'City', type: 'text' },
    { field: 'E-mail', type: 'email' },
    { field: 'Whatsapp', type: 'number' },
  ];

  $scope.topics = [{
      subject: 'Your Identity',
      questions: [{
        question: 'Tell me about your company/project/yourself.',
        commentary: 'Include the full name you want to use on the site, your mission/tagline (if you have one), and anything else you think I should know about you.'
      }, {
        question: 'Do you have any existing branding you want to incorporate into the website design?',
        commentary: 'This could include a colour palette, logo, fonts, print material, ads, etc. Feel free to send me any relevant documents/images related to this as well'
      }, {
        question: 'Do you have a font preference?',
        commentary: 'There is a huge range of fonts available for use on the web, but some cost more to license than others. If there are any particular font families or styles of fonts you like, I can narrow down the selection. It can even be useful to know whether you generally prefer serif fonts (with the little feet) or sans-serif (like this one, without the little feet).'
      }, {
        question: 'Tell me about your company/project/yourself.',
        commentary: 'Include the full name you want to use on the site, your mission/tagline (if you have one), and anything else you think I should know about you.'
      }, {
        question: 'Who is your target audience?',
        commentary: 'Describe their age, interests, industry – anything relevant to the design of the site.'
      }, {
        question: 'Tell me about your competitors.',
        commentary: 'What do you do differently? Is there a specific selling point that sets you apart from them?'
      }, ]
    },

    {
      subject: 'Your Project',
      questions: [{
          question: 'What are your goals for this project?',
          commentary: 'These could include selling a product, connecting more directly with customers, presenting your company’s personality in a certain way, advertising upcoming events, etc.'
        }, {
          question: 'What result would make you consider this project a success? Do you have a way to quantify or measure this result?',
          commentary: 'You’ve listed your goals for this project; now consider how you’ll know when those goals have been met. Measurable results are the best way for both of us to stay on track.'
        }, {
          question: 'If this is a redesign of an existing app/site, what are the main problems with your current website that you’d likethis redesign to address?',
          commentary: 'For example: a more professional look, a clearer call-to-action, making your services section more prominent, a pareddown navigation menu, etc.'
        }, {
          question: 'List all of the content and functionality you want, in as much detail as possible',
          commentary: 'For example: a front page with a blog that visitors can comment on, an about page with an author photo and bio, a contact form with drop-down menus to select a specific recipient, etc.'
        }, {
          question: 'List some keywords that describe the feel of your ideal website/app. ',
          commentary: 'For example: minimalist, bold, colourful, warm, artsy, corporate, soothing, etc'
        }, {
          question: 'Show me a few you really like.',
          commentary: 'List in as much detail as possible what it is you like about them. For example: use of photography, font choices, page layout, etc. If there are things about them you do not like, please specify those as well!'
        },



      ]
    },



  ]






}])