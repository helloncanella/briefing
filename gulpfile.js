var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
 

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync.init(null, {
    proxy: "http://localhost:9000",
    notify: false,
    files: ["app/**/*.*"],
    port: 7000,
  });
});
gulp.task('nodemon', function (cb) {
  
  var started = false;
  
  return $.nodemon({
    script: 'index.js'
  }).on('start', function () {
    // to avoid nodemon being started multiple times
    // thanks @matthisk
    if (!started) {
      cb();
      started = true; 
    } 
  });
});

gulp.task('sass', function() {

  return gulp
    .src('./app/components/bundle.scss',{base:'./'})
    .pipe($.sassGlob())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer({
      // browsers: ['iOS > 7', 'ie >= 11', 'and_chr >= 2.3'],
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('.'))
    .pipe(browserSync.stream());
});

gulp.task('default', ['sass', 'browser-sync'], function() {
  gulp.watch(['**/*.scss'], ['sass']);
  gulp.watch(['app/index.html','app/**/*.js']).on('change', browserSync.reload);
});




function errFunction(err, data) {
  if (err) console.log(err);
}